/*$off*/
/*$4 *******************************************************************************************************************
*
* DESCRIPTION:  external library - expose .h files
*
* AUTHOR:       robert.berger@reliableembeddedsystems.com
*
* FILENAME:     depends.h
*
* REMARKS:      this .h file is a public/external API
*
* HISTORY:      001b,21 Nov 2020,rber    renamed a few things
*               001a,14 Feb 2020,rber    written 
*
* COPYRIGHT:    (C) 2020
*
 *********************************************************************************************************************** */
/*$on*/

#ifndef __LIB_DEPENDS_H__
#define __LIB_DEPENDS_H__ 1

/*$3 ===================================================================================================================
    $C                                             Included headers
 ======================================================================================================================= */

#include <stdio.h>

/* Included headers before this point */
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*$3 ===================================================================================================================
    $C                                                  Macros
 ======================================================================================================================= */

/* differentiate between public and private prototypes */
    #if defined(LIB_DEPENDS_PRIVATE_PROTOTYPES)
        #define EXTERN
    #else
        #define EXTERN  extern
    #endif

/*$3 ===================================================================================================================
    $C                                       Public function declarations
 ======================================================================================================================= */

    EXTERN void hello_from_dependslib(void);

#undef EXTERN

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __LIB_DEPENDS_H__ */

/* 
 * EOF 
 */

