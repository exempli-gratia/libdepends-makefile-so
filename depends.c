/* this is my libdepends */

/* only here it's actually defined a not extern */
#define LIB_DEPENDS_PRIVATE_PROTOTYPES
#include "depends.h"
#undef LIB_DEPENDS_PRIVATE_PROTOTYPES

void hello_from_dependslib() {
  printf("Hello from dependslib!\n");
}
